package com.treinamento.MiniSIGA.repository;
import com.treinamento.MiniSIGA.entity.CursoUFRJ;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CursoUFRJRepository extends JpaRepository<CursoUFRJ, String> {

    CursoUFRJ getCursoUFRJById(String id);

    void deleteCursoUFRJById(String id);

    void deleteCursoUFRJByCodigoMEC(String codigoMEC);
}
