package com.treinamento.MiniSIGA.service;
import com.treinamento.MiniSIGA.entity.CursoUFRJ;
import com.treinamento.MiniSIGA.repository.CursoUFRJRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class CursoUFRJService {
    @Autowired
    CursoUFRJRepository cursoUFRJRepository;

    public CursoUFRJ getCursoUFRJById(String id){
        return cursoUFRJRepository.getCursoUFRJById(id);
    }

    public List<CursoUFRJ> getCursoUFRJ(){
        return cursoUFRJRepository.findAll();
    }

    public CursoUFRJ cadastrar(CursoUFRJ cursoUFRJ){
        return cursoUFRJRepository.save(cursoUFRJ);
    }

    public CursoUFRJ update(CursoUFRJ cursoUFRJ){
        return cursoUFRJRepository.save(cursoUFRJ);
    }

    @Transactional
    public void deleteCursoUFRJById(String id){
        cursoUFRJRepository.deleteCursoUFRJById(id);
    }

    @Transactional
    public void deleteCursoUFRJByCodigo(String codigoMEC){
        cursoUFRJRepository.deleteCursoUFRJByCodigoMEC(codigoMEC);
    }

    
}
