package com.treinamento.MiniSIGA.controller;
import com.treinamento.MiniSIGA.entity.Professor;
import com.treinamento.MiniSIGA.service.ProfessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/professor")
public class ProfessorController {

    @Autowired
    private ProfessorService professorService;

    //Create
    @PostMapping(path = "/cadastrar", consumes = "application/json")
    public Professor cadastrar(@RequestBody Professor professor){
        return professorService.cadastrar(professor);
    }

    //Read
    @GetMapping(path = "/id/{id}", produces = "application/json")
    public Professor getProfessorById(@PathVariable String id){
        return professorService.getProfessorById(id);
    }

    //update (PUT)
    @PutMapping(path = "/atualizar", consumes = "application/json")
    public Professor update(@RequestBody Professor professor){
        return professorService.update(professor);
    }

    //DELETE
    @DeleteMapping(path = "/deletar/{id}")
    public void deletar(@PathVariable String id){
        professorService.deleteHistoricoById(id);
    }

}
