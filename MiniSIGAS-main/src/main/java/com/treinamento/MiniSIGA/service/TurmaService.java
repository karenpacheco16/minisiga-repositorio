package com.treinamento.MiniSIGA.service;
import com.treinamento.MiniSIGA.entity.Turma;
import com.treinamento.MiniSIGA.repository.TurmaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TurmaService {

    @Autowired
    TurmaRepository turmaRepository;

    public Turma getTurmaById(String id){
        return turmaRepository.getTurmaById(id);
    }

    public List<Turma> getTurmas(){
        return turmaRepository.findAll();
    }

    public Turma cadastrar(Turma turma){
        return turmaRepository.save(turma);
    }

    public Turma update(Turma turma){
        return turmaRepository.save(turma);
    }

    @Transactional
    public void deleteTurmaById(String id){
        turmaRepository.deleteTurmaById(id);
    }

    
}
