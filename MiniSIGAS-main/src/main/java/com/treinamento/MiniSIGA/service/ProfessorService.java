package com.treinamento.MiniSIGA.service;
import com.treinamento.MiniSIGA.entity.Professor;
import com.treinamento.MiniSIGA.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;


@Service
public class ProfessorService {
    @Autowired
    ProfessorRepository professorRepository;

    public Professor cadastrar(Professor professor){
        return professorRepository.save(professor);
    }

    public Professor getProfessorById(String id){
        return professorRepository.getProfessorById(id);
    }
    public Professor update (Professor professor){
        return professorRepository.save(professor);
    }

    @Transactional
    public void deleteHistoricoById(String id){
        professorRepository.deleteHistoricoById(id);
    }
    
}
