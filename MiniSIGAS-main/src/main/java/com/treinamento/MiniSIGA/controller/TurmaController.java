package com.treinamento.MiniSIGA.controller;
import com.treinamento.MiniSIGA.entity.Turma;
import com.treinamento.MiniSIGA.service.TurmaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/turma")
public class TurmaController {

    @Autowired
    private TurmaService turmaService;

    //Create
    @PostMapping(path = "/cadastrar", consumes = "application/json")
    public Turma cadastrar(@RequestBody Turma turma){
        return turmaService.cadastrar(turma);
    }

    //Read
    @GetMapping(path = "/id/{id}", produces = "application/json")
    public Turma getTurmaById(@PathVariable String id){
        return turmaService.getTurmaById(id);
    }

    //update (PUT)
    @PutMapping(path = "/atualizar", consumes = "application/json")
    public Turma update(@RequestBody Turma turma){
        return turmaService.update(turma);
    }

    //DELETE
    @DeleteMapping(path = "/deletar/{id}")
    public void deletar(@PathVariable String id){
        turmaService.deleteTurmaById(id);
    }

}
