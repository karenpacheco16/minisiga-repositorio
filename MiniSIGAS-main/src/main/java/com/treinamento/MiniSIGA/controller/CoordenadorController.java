package com.treinamento.MiniSIGA.controller;
import com.treinamento.MiniSIGA.entity.Coordenador;
import com.treinamento.MiniSIGA.service.CoordenadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/coordenador")
public class CoordenadorController {

    @Autowired
    private CoordenadorService coordenadorService;

    //Create
    @PostMapping(path = "/cadastrar", consumes = "application/json")
    public Coordenador cadastrar(@RequestBody Coordenador coordenador){
        return coordenadorService.cadastrar(coordenador);
    }

    //Read
    @GetMapping(path = "/id/{id}", produces = "application/json")
    public Coordenador getCoordenadorById(@PathVariable String id){
        return coordenadorService.getCoordenadorById(id);
    }

    //update (PUT)
    @PutMapping(path = "/atualizar", consumes = "application/json")
    public Coordenador update(@RequestBody Coordenador coordenador){
        return coordenadorService.update(coordenador);
    }

    //DELETE
    @DeleteMapping(path = "/deletar/{id}")
    public void deletar(@PathVariable String id){
        coordenadorService.deleteCoordenadorById(id);
    }
}
