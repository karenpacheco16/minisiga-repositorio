package com.treinamento.MiniSIGA.service;
import com.treinamento.MiniSIGA.entity.Disciplina;
import com.treinamento.MiniSIGA.repository.DisciplinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class DisciplinaService {

    @Autowired
    DisciplinaRepository disciplinaRepository;

    public Disciplina getDisciplinaById(String id){
        return disciplinaRepository.getDisciplinaUFRJById(id);
    }

    public Disciplina cadastrar (Disciplina disciplina){
        return disciplinaRepository.save(disciplina);
    }

    public Disciplina update (Disciplina disciplina){
        return disciplinaRepository.save(disciplina);
    }
    @Transactional
    public void deleteDisciplinaById (String id){
        disciplinaRepository.deleteDisciplinaById(id);
    }

}
