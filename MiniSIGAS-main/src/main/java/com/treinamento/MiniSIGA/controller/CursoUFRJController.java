package com.treinamento.MiniSIGA.controller;
import com.treinamento.MiniSIGA.entity.CursoUFRJ;
import com.treinamento.MiniSIGA.service.CursoUFRJService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/cursoUFRJ")
public class CursoUFRJController {

    @Autowired
    private CursoUFRJService cursoUFRJService;

    //Create
    @PostMapping(path = "/cadastrar", consumes = "application/json")
    public CursoUFRJ cadastrar(@RequestBody CursoUFRJ cursoUFRJ){
        return cursoUFRJService.cadastrar(cursoUFRJ);
    }

    //Read
    @GetMapping(path = "/id/{id}", produces = "application/json")
    public CursoUFRJ getCursoById(@PathVariable String id){
        return cursoUFRJService.getCursoUFRJById(id);
    }

    //update (PUT)
    @PutMapping(path = "/atualizar", consumes = "application/json")
    public CursoUFRJ update(@RequestBody CursoUFRJ cursoUFRJ){
        return cursoUFRJService.update(cursoUFRJ);
    }

    //DELETE
    @DeleteMapping(path = "/deletar/{id}")
    public void deleteCursoUFRJById(@PathVariable String id){
        cursoUFRJService.deleteCursoUFRJById(id);
    }
}
