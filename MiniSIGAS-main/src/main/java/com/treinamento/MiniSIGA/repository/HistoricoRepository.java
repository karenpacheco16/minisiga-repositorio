package com.treinamento.MiniSIGA.repository;
import com.treinamento.MiniSIGA.entity.Historico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoricoRepository extends JpaRepository<Historico, String> {

    Historico getHistoricoById(String id);
    void deleteHistoricoById(String id);
}
