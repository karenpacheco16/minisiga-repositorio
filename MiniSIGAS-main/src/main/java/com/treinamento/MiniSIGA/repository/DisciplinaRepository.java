package com.treinamento.MiniSIGA.repository;
import com.treinamento.MiniSIGA.entity.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DisciplinaRepository extends JpaRepository<Disciplina, String> {
    Disciplina getDisciplinaUFRJById(String cpf);

    void deleteDisciplinaById(String id);
}
