package com.treinamento.MiniSIGA.entity;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "Historico", schema = "karen")
public class Historico {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @OneToOne
    @JoinColumn(name = "aluno_id")
    private Aluno aluno;

    @Basic
    @Column(name = "dataIngresso")
    private Date dataIngresso;

    @Basic
    @Column(name = "cr_medio")
    private Float cr_medio;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Date getDataIngresso() {
        return dataIngresso;
    }

    public void setDataIngresso(Date dataIngresso) {
        this.dataIngresso = dataIngresso;
    }

    public Float getCr_medio() {
        return cr_medio;
    }

    public void setCr_medio(Float cr_medio) {
        this.cr_medio = cr_medio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Historico historico = (Historico) o;
        return id.equals(historico.id) && Objects.equals(aluno, historico.aluno);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, aluno);
    }
}
