package com.treinamento.MiniSIGA.service;
import com.treinamento.MiniSIGA.entity.Historico;
import com.treinamento.MiniSIGA.repository.HistoricoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class HistoricoService {
    @Autowired
    HistoricoRepository historicoRepository;

    public Historico getHistoricoById(String id){
        return historicoRepository.getHistoricoById(id);
    }
    public Historico cadastrar(Historico historico){
        return historicoRepository.save(historico);
    }
    public Historico update (Historico historico){
        return historicoRepository.save(historico);
    }

    @Transactional
    public void deleteHistoricoById(String id){
        historicoRepository.deleteHistoricoById(id);
    }
}
