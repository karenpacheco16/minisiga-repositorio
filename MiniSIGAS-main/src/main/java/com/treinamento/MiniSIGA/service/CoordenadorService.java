package com.treinamento.MiniSIGA.service;
import com.treinamento.MiniSIGA.entity.Coordenador;
import com.treinamento.MiniSIGA.repository.CoordenadorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
public class CoordenadorService {

    @Autowired
    private CoordenadorRepository coordenadorRepository;

    public Coordenador getCoordenadorById(String id){
        return coordenadorRepository.getCoordenadorById(id);
    }

    public List<Coordenador> getCoordenadores(){
        return coordenadorRepository.findAll();
    }

    public Coordenador cadastrar(Coordenador coordenador){
        return coordenadorRepository.save(coordenador);
    }

    public Coordenador update(Coordenador coordenador){
        return coordenadorRepository.save(coordenador);
    }

    @Transactional
    public void deleteCoordenadorById(String id){
        coordenadorRepository.deleteCoordenadorById(id);
    }



}
