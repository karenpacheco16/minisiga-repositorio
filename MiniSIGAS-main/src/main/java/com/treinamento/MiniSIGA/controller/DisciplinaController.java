package com.treinamento.MiniSIGA.controller;
import com.treinamento.MiniSIGA.entity.Disciplina;
import com.treinamento.MiniSIGA.service.DisciplinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/disciplina")
public class DisciplinaController {

    @Autowired
    private DisciplinaService disciplinaService;

    //Create
    @PostMapping(path = "/cadastrar", consumes = "application/json")
    public Disciplina cadastrar(@RequestBody Disciplina disciplina){
        return disciplinaService.cadastrar(disciplina);
    }

    //Read
    @GetMapping(path = "/id/{id}", produces = "application/json")
    public Disciplina getDisciplinaById(@PathVariable String id){
        return disciplinaService.getDisciplinaById(id);
    }

    //update (PUT)
    @PutMapping(path = "/atualizar", consumes = "application/json")
    public Disciplina update(@RequestBody Disciplina disciplina){
        return disciplinaService.update(disciplina);
    }

    //DELETE
    @DeleteMapping(path = "/deletar/{id}")
    public void deletar(@PathVariable String id){
        disciplinaService.deleteDisciplinaById(id);
    }
}
