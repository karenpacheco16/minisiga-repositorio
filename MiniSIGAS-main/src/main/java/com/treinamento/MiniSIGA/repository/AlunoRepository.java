package com.treinamento.MiniSIGA.repository;
import com.treinamento.MiniSIGA.entity.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlunoRepository extends JpaRepository<Aluno, String> {

    Aluno getAlunoByCpf(String cpf);

    void deleteByMatriculaDre(String matriculaDre);

    void deleteByCpf(String cpf);
}
