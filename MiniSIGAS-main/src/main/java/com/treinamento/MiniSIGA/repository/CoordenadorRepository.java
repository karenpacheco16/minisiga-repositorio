package com.treinamento.MiniSIGA.repository;
import com.treinamento.MiniSIGA.entity.Coordenador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoordenadorRepository extends JpaRepository<Coordenador, String> {

    Coordenador getCoordenadorById(String id);

    void deleteCoordenadorById(String id);

}
