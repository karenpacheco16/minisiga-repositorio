package com.treinamento.MiniSIGA.repository;
import com.treinamento.MiniSIGA.entity.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, String> {
    Professor getProfessorById(String id);
    void deleteHistoricoById(String id);

}
