package com.treinamento.MiniSIGA.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;

import java.util.List;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Aluno", schema = "karen")

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")

public class Aluno {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @Basic
    @Column(name = "nome")
    private String nome;

    @Column(name = "cpf", updatable = false, nullable = false, unique = true, length = 11)
    private String cpf;

    @Basic
    @Column(name = "matriculaDRE")
    private String matriculaDre;

    @ManyToOne
    @JoinColumn(name = "curso_id")
    private CursoUFRJ curso;

    @ManyToMany
    @JoinTable (name="AlunoTurma", joinColumns =@JoinColumn(name = "aluno_id"),
    inverseJoinColumns = @JoinColumn(name = "turma_id"))
    private List<Turma> turmas;

   
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getMatriculaDre() {
        return matriculaDre;
    }

    public void setMatriculaDre(String matriculaDre) {
        this.matriculaDre = matriculaDre;
    }

    public CursoUFRJ getCurso() {
        return curso;
    }

    public void setCurso(CursoUFRJ curso) {
        this.curso = curso;
    }


    public List<Turma> getTurmas() {
        return turmas;
    }

    public void setTurmas(List<Turma> turmas) {
        this.turmas = turmas;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Aluno that = (Aluno) o;
        return Objects.equals(id, that.id) && Objects.equals(nome, that.nome) && Objects.equals(cpf, that.cpf) && Objects.equals(matriculaDre, that.matriculaDre) && Objects.equals(curso, that.curso);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, cpf, matriculaDre, curso);
    }
}
