package com.treinamento.MiniSIGA.entity;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "CursoUFRJ", schema = "karen")
public class CursoUFRJ {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @Basic
    @Column(name = "nome")
    private String nome;

    @Basic
    @Column(name = "nível")
    private String nível;

    @Basic
    @Column(name = "unidade")
    private String unidade;

    @Basic
    @Column(name = "codigoMEC")
    private String codigoMEC;

    @CreatedDate
    @Column(name = "data_criacao")
    private Date data_criacao;

    @Basic
    @Column(name = "ativo")
    private Boolean ativo;


    @OneToOne
    @JoinColumn(name = "coordenador_id")
    private Coordenador coordenador;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNível() {
        return nível;
    }

    public void setNível(String nível) {
        this.nível = nível;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getCodigoMEC() {
        return codigoMEC;
    }

    public void setCodigoMEC(String codigoMEC) {
        this.codigoMEC = codigoMEC;
    }

    public Date getData_criacao() {
        return data_criacao;
    }

    public void setData_criacao(Date data_criacao) {
        this.data_criacao = data_criacao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Coordenador getCoordenador() {
        return coordenador;
    }

    public void setCoordenador(Coordenador coordenador) {
        this.coordenador = coordenador;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CursoUFRJ that = (CursoUFRJ) o;
        return Objects.equals(id, that.id) && Objects.equals(codigoMEC, that.codigoMEC);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, nível,unidade,codigoMEC,data_criacao,ativo,coordenador);
    }


}
