package com.treinamento.MiniSIGA.controller;
import com.treinamento.MiniSIGA.entity.Historico;
import com.treinamento.MiniSIGA.service.HistoricoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/historico")
public class HistoricoController {

    @Autowired
    private HistoricoService historicoService;

    //Create
    @PostMapping(path = "/cadastrar", consumes = "application/json")
    public Historico cadastrar(@RequestBody Historico historico){
        return historicoService.cadastrar(historico);
    }

    //Read
    @GetMapping(path = "/id/{id}", produces = "application/json")
    public Historico getHistoricoById(@PathVariable String id){
        return historicoService.getHistoricoById(id);
    }

    //update (PUT)
    @PutMapping(path = "/atualizar", consumes = "application/json")
    public Historico update(@RequestBody Historico historico){
        return historicoService.update(historico);
    }

    //DELETE
    @DeleteMapping(path = "/deletar/{id}")
    public void deletar(@PathVariable String id){
        historicoService.deleteHistoricoById(id);
    }
}
